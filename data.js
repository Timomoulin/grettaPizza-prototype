var ingredient1= { idIngredient: 1, nomIngredient: 'tomate' ,ingredients:[]};
var ingredient2= { idIngredient: 2, nomIngredient: 'fromage' ,ingredients:[]};
var ingredient3= { idIngredient: 3, nomIngredient: 'jambon' ,ingredients:[]};
var ingredient4= { idIngredient: 4, nomIngredient: 'emmental' ,ingredients:[]};
var ingredient5= { idIngredient: 5, nomIngredient: 'bœuf' ,ingredients:[]};
var ingredient6= { idIngredient: 6, nomIngredient: 'oignons' ,ingredients:[]};
var ingredient7= {idIngredient: 7,nomIngredient: 'jambon ou thon ou poulet rôti maison',ingredients:[]};
var ingredient8= { idIngredient: 8, nomIngredient: 'champignons' ,ingredients:[]};
var ingredient9= { idIngredient: 9, nomIngredient: 'pepperoni' ,ingredients:[]};
var ingredient10= { idIngredient: 10, nomIngredient: 'crème fraîche' ,ingredients:[]};
var ingredient11= { idIngredient: 11, nomIngredient: 'pommes de terre' ,ingredients:[]};
var ingredient12= { idIngredient: 12, nomIngredient: 'lardons' ,ingredients:[]};
var ingredient13= { idIngredient: 13, nomIngredient: 'gros chorizo' ,ingredients:[]};
var ingredient14= { idIngredient: 14, nomIngredient: 'cheddar' ,ingredients:[]};
var ingredient15= { idIngredient: 15, nomIngredient: 'kebab' ,ingredients:[]};
var ingredient16= { idIngredient: 16, nomIngredient: 'tomates fraîches' ,ingredients:[]};
var ingredient17= { idIngredient: 17, nomIngredient: 'féta' ,ingredients:[]};
var ingredient18= {idIngredient: 18,nomIngredient: 'aubergines grillées',ingredients:[]};
var ingredient19= { idIngredient: 19, nomIngredient: 'câpres' ,ingredients:[]};
var ingredient20= { idIngredient: 20, nomIngredient: 'olives' ,ingredients:[]};
var ingredient21= { idIngredient: 21, nomIngredient: 'anchois' ,ingredients:[]};
var ingredient22= {idIngredient: 22,nomIngredient: 'Jambon ou poulet rôti maison',ingredients:[]};
var ingredient23= {idIngredient: 23,nomIngredient: "jeunes pousses d'épinard",ingredients:[]};
var ingredient24= { idIngredient: 24, nomIngredient: 'boursin' ,ingredients:[]};
var ingredient25= { idIngredient: 25, nomIngredient: 'base moutarde' ,ingredients:[]};
var ingredient26= {idIngredient: 26,nomIngredient: 'épices mexicaines',ingredients:[]};
var ingredient27= { idIngredient: 27, nomIngredient: 'poulet tikka' ,ingredients:[]};
var ingredient28= {idIngredient: 28,nomIngredient: 'sauce pimentée maison',ingredients:[]};
var ingredient29= { idIngredient: 29, nomIngredient: 'bacon' ,ingredients:[]};
var ingredient30= { idIngredient: 30, nomIngredient: 'œuf' ,ingredients:[]};
var ingredient31= { idIngredient: 31, nomIngredient: 'merguez' ,ingredients:[]};
var ingredient32= { idIngredient: 32, nomIngredient: 'ananas' ,ingredients:[]};
var ingredient33= { idIngredient: 33, nomIngredient: 'maïs' ,ingredients:[]};
var ingredient34= { idIngredient: 34, nomIngredient: 'poivrons' ,ingredients:[]};
var ingredient35= { idIngredient: 35, nomIngredient: 'thon' ,ingredients:[]};
var ingredient36= { idIngredient: 36, nomIngredient: 'chèvre' ,ingredients:[]};
var ingredient37= { idIngredient: 37, nomIngredient: 'sauce barbecue' ,ingredients:[]};
var ingredient38= {idIngredient: 38,nomIngredient: 'cocktail de fruits de mer',ingredients:[]};
var ingredient39= {idIngredient: 39,nomIngredient: 'persillade maison',ingredients:[]};
var ingredient40= { idIngredient: 40, nomIngredient: 'saumon' ,ingredients:[]};
var ingredient41= {idIngredient: 41,nomIngredient: 'poulet rôti maison ou lardons',ingredients:[]};
var ingredient42= { idIngredient: 42, nomIngredient: 'miel' ,ingredients:[]};
var ingredient43= { idIngredient: 43, nomIngredient: "jaune d'oeuf" ,ingredients:[]};
var ingredient44= { idIngredient: 44, nomIngredient: 'asperges' ,ingredients:[]};
var ingredient45= { idIngredient: 45, nomIngredient: 'citron' ,ingredients:[]};
var ingredient46= {idIngredient: 46,nomIngredient: 'artichauts à la romaine',ingredients:[]};
var ingredient47= { idIngredient: 47, nomIngredient: 'double fromage' ,ingredients:[]};
var ingredient48= { idIngredient: 48, nomIngredient: 'bleu' ,ingredients:[]};
var ingredient49= {idIngredient: 49,nomIngredient: 'fromage à raclette',ingredients:[]};
var ingredient50= { idIngredient: 50, nomIngredient: 'jambon de pays' ,ingredients:[]};
var ingredient51= { idIngredient: 51, nomIngredient: 'cornichons' ,ingredients:[]};
var ingredient52= { idIngredient: 52, nomIngredient: 'curry' ,ingredients:[]};
var ingredient53= {idIngredient: 53,nomIngredient: 'poulet rôti maison',ingredients:[]};
var ingredient54= { idIngredient: 54, nomIngredient: 'tapenade' ,ingredients:[]};
var ingredient55= { idIngredient: 55, nomIngredient: 'parmesan' ,ingredients:[]};
var ingredient56= { idIngredient: 56, nomIngredient: 'reblochon' ,ingredients:[]};
var ingredient57= { idIngredient: 57, nomIngredient: 'St-Jacques' ,ingredients:[]};
var ingredient58= { idIngredient: 58, nomIngredient: 'roquette' ,ingredients:[]};
var ingredient59= { idIngredient: 59, nomIngredient: 'chicorée' ,ingredients:[]};
var ingredient60= { idIngredient: 60, nomIngredient: 'tomates confites' ,ingredients:[]};
var ingredient61= {idIngredient: 61,nomIngredient: 'crème de balsamique',ingredients:[]};
var ingredient62= {idIngredient: 62,nomIngredient: 'saucisse spianata piccante',ingredients:[]};
var ingredient63= { idIngredient: 63, nomIngredient: 'pesto' ,ingredients:[]};
var ingredient64= {idIngredient: 64,nomIngredient: 'aubergines grillées marinées',ingredients:[]};
var ingredient65= { idIngredient: 65, nomIngredient: 'ricotta' ,ingredients:[]};
var ingredient66= {idIngredient: 66,nomIngredient: 'aubergines grillées maison',ingredients:[]};
var ingredient67= { idIngredient: 67, nomIngredient: 'gorgonzola' ,ingredients:[]};
var ingredient68= {idIngredient: 68,nomIngredient: 'base légumes maison',ingredients:[]};
var ingredient69= {idIngredient: 69,nomIngredient: 'après cuisson : parmesan',ingredients:[]};
var ingredient70= {idIngredient: 70,nomIngredient: 'Après cuisson : crème de balsamique',ingredients:[]};
var ingredient71= {idIngredient: 71,nomIngredient: 'graines de sésame',ingredients:[]};
var ingredient72= { idIngredient: 72, nomIngredient: 'poulet' ,ingredients:[]};

pizza1 = {
    idPizza: 1,
    nomPizza: 'GOURMET(Margarita)',
    vegetarienne: 1,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 1
  ,ingredients:[ingredient1,ingredient2]};
  pizza2 = {
    idPizza: 2,
    nomPizza: 'BOSTON',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 2
  ,ingredients:[ingredient1,ingredient2,ingredient3,ingredient4]};
  pizza3 = {
    idPizza: 3,
    nomPizza: 'NEW YORK STYLE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 2
  ,ingredients:[ingredient1,ingredient2,ingredient5,ingredient6]};
  pizza4 = {
    idPizza: 4,
    nomPizza: 'CALZONE (soufflée ou non)',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 2
  ,ingredients:[ingredient1,ingredient2,ingredient7,ingredient30]};
  pizza5 = {
    idPizza: 5,
    nomPizza: 'TEXAS',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 2
  ,ingredients:[ingredient1,ingredient2,ingredient3,ingredient8]};
  pizza6 = {
    idPizza: 6,
    nomPizza: 'MIAMI',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 2
  ,ingredients:[ingredient1,ingredient2,ingredient8,ingredient30]};
  pizza7 = {
    idPizza: 7,
    nomPizza: 'FERMIÈRE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 2
  ,ingredients:[ingredient2,ingredient10,ingredient11,ingredient12]};
  pizza8 = {
    idPizza: 8,
    nomPizza: 'IBIZA',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 2
  ,ingredients:[ingredient1,ingredient2,ingredient3,ingredient13]};
  pizza9 = {
    idPizza: 9,
    nomPizza: 'VEGAS',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 2
  ,ingredients:[ingredient1,ingredient2,ingredient5,ingredient14]};
  pizza10 = {
    idPizza: 10,
    nomPizza: 'KEBAB',
    vegetarienne: 0,
    new: 1,
    nomGenre: 'Nos classics',
    idTarif: 2
  ,ingredients:[ingredient6,ingredient10,ingredient15]};
  pizza11 = {
    idPizza: 11,
    nomPizza: 'ITALIENNE',
    vegetarienne: 1,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[ingredient1,ingredient2,ingredient16,ingredient17,ingredient18]};
  pizza12 = {
    idPizza: 12,
    nomPizza: 'NAPOLITAINE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[ingredient1,ingredient2,ingredient19,ingredient20,ingredient21]};
  pizza48 = {
    idPizza: 48,
    nomPizza: 'OZOIRIENNE',
    vegetarienne: 1,
    new: 0,
    nomGenre: 'Nos pizzas Italiennes',
    idTarif: 4
  ,ingredients:[ingredient1,ingredient2,ingredient36,ingredient58,ingredient60,ingredient61]};
  pizza49 = {
    idPizza: 49,
    nomPizza: 'SPIANATA',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos pizzas Italiennes',
    idTarif: 4
  ,ingredients:[ingredient2,ingredient6,ingredient8,ingredient10,ingredient62,ingredient63]};
  pizza50 = {
    idPizza: 50,
    nomPizza: 'TUTTA VÉGÉTALE',
    vegetarienne: 1,
    new: 0,
    nomGenre: 'Nos pizzas Italiennes',
    idTarif: 4
  ,ingredients:[ingredient1,ingredient2,ingredient16,ingredient44,ingredient63,ingredient64]};
  pizza51 = {
    idPizza: 51,
    nomPizza: 'RICOTTA',
    vegetarienne: 1,
    new: 0,
    nomGenre: 'Nos pizzas Italiennes',
    idTarif: 4
  ,ingredients:[ingredient1,ingredient2,ingredient63,ingredient65,ingredient66]};
  pizza52 = {
    idPizza: 52,
    nomPizza: 'GORGONZOLA',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos pizzas Italiennes',
    idTarif: 4
  ,ingredients:[ingredient1,ingredient2,ingredient50,ingredient60,ingredient63,ingredient67]};
  pizza53 = {
    idPizza: 53,
    nomPizza: 'JACQUES',
    vegetarienne: 1,
    new: 1,
    nomGenre: 'Nos pizzas Italiennes',
    idTarif: 4
  ,ingredients:[ingredient66,ingredient68,ingredient69,ingredient70]};
  pizza54 = {
    idPizza: 54,
    nomPizza: 'CALZONNETA(Soufflée)',
    vegetarienne: 1,
    new: 1,
    nomGenre: 'Nos pizzas Italiennes',
    idTarif: 4
  ,ingredients:[ingredient2,ingredient17,ingredient43,ingredient58,ingredient60,ingredient68,ingredient71]};
  
var lesPizzas=[
    pizza1,
      pizza2,
      pizza3,
      pizza4,
      pizza5,
      pizza6,
      pizza7 ,
      pizza8 ,
      pizza9,
      pizza10,
      pizza11,
      pizza12,
      pizza48,
      pizza49,
      pizza50,
      pizza51,
      pizza52,
      pizza53,
      pizza54,
];
  /*
  pizza13 = {
    idPizza: 13,
    nomPizza: 'WASHINGTON',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[]};
  pizza14 = {
    idPizza: 14,
    nomPizza: 'DIJONNAISE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[]};
  pizza15 = {
    idPizza: 15,
    nomPizza: 'CALIENTE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[]};
  pizza16 = {
    idPizza: 16,
    nomPizza: 'DALLAS',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[]};
  pizza17 = {
    idPizza: 17,
    nomPizza: 'AUSTRALIENNE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[]};
  pizza18 = {
    idPizza: 18,
    nomPizza: 'ASIATIQUE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[]};
  pizza19 = {
    idPizza: 19,
    nomPizza: 'EUROPÉENNE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[]};
  pizza20 = {
    idPizza: 20,
    nomPizza: 'HAWAÏENNE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[]};
  pizza21 = {
    idPizza: 21,
    nomPizza: 'LÉSIGNY',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[]};
  pizza22 = {
    idPizza: 22,
    nomPizza: 'PAYSANNE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[]};
  pizza23 = {
    idPizza: 23,
    nomPizza: 'OCÉANE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[]};
  pizza24 = {
    idPizza: 24,
    nomPizza: 'ORIENTALE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[]};
  pizza25 = {
    idPizza: 25,
    nomPizza: 'FORESTIÈRE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[]};
  pizza26 = {
    idPizza: 26,
    nomPizza: 'BUFFALO',
    vegetarienne: 0,
    new: 1,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[]};
  pizza27 = {
    idPizza: 27,
    nomPizza: 'MÉDITERRANÉE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[]};
  pizza28 = {
    idPizza: 28,
    nomPizza: 'NORVÉGIENNE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[]};
  pizza29 = {
    idPizza: 29,
    nomPizza: 'SUÉDOISE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[]};
  pizza30 = {
    idPizza: 30,
    nomPizza: 'CANADIENNE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 3
  ,ingredients:[]};
  pizza31 = {
    idPizza: 31,
    nomPizza: 'CALZONE DU CHEF',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 4
  ,ingredients:[]};
  pizza32 = {
    idPizza: 32,
    nomPizza: '4 SAISONS',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 4
  ,ingredients:[]};
  pizza33 = {
    idPizza: 33,
    nomPizza: 'AMÉRICAINE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 4
  ,ingredients:[]};
  pizza34 = {
    idPizza: 34,
    nomPizza: 'BOLOGNAISE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 4
  ,ingredients:[]};
  pizza35 = {
    idPizza: 35,
    nomPizza: 'MONTANA',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 4
  ,ingredients:[]};
  pizza36 = {
    idPizza: 36,
    nomPizza: 'FRANÇAISE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 4
  ,ingredients:[]};
  pizza37 = {
    idPizza: 37,
    nomPizza: '4 FROMAGES',
    vegetarienne: 1,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 4
  ,ingredients:[]};
  pizza38 = {
    idPizza: 38,
    nomPizza: 'VÉGÉTARIENNE',
    vegetarienne: 1,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 4
  ,ingredients:[]};
  pizza39 = {
    idPizza: 39,
    nomPizza: 'RACLETTE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 4
  ,ingredients:[]};
  pizza40 = {
    idPizza: 40,
    nomPizza: 'MEXICAINE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 4
  ,ingredients:[]};
  pizza41 = {
    idPizza: 41,
    nomPizza: 'INDIENNE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 4
  ,ingredients:[]};
  pizza42 = {
    idPizza: 42,
    nomPizza: 'CHICAGO',
    vegetarienne: 1,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 4
  ,ingredients:[]};
  pizza43 = {
    idPizza: 43,
    nomPizza: 'GOURMANDE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 4
  ,ingredients:[]};
  pizza44 = {
    idPizza: 44,
    nomPizza: 'TARTIFLETTE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 4
  ,ingredients:[]};
  pizza45 = {
    idPizza: 45,
    nomPizza: 'CAMPIONE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 4
  ,ingredients:[]};
  pizza46 = {
    idPizza: 46,
    nomPizza: 'COMPOSTELLE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos classics',
    idTarif: 4
  ,ingredients:[]};
  pizza47 = {
    idPizza: 47,
    nomPizza: 'SANTENOISE',
    vegetarienne: 0,
    new: 0,
    nomGenre: 'Nos pizzas Italiennes',
    idTarif: 4
  ,ingredients:[ingredient1,ingredient2,ingredient50,ingredient55,ingredient58]};*/
  