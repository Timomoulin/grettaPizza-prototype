var divpizzasClassics = document.querySelector("#pizzasClassics"); /* Ici je crée une variable qui corespond à la balise ou je veux envoyer les infos pour les pizzas Classic */
var divpizzaItaliennes = document.querySelector("#pizzaItaliennes");/* Ici je crée une variable qui corespond à la balise ou je veux envoyer les infos pour les pizzas Italiennes */

function affichePizzas(desPizzas) /*Je crée une procedure affichePizza qui reçoit un parametre desPizza (une liste de pizzas) */
{
    var chainePizzaClassics=""; 
    /* J'initialise une chaine de caractère que je vais remplir a chaque passage dans la boucle SI la pizza que je suis entrain de parcourir et une pizza Classique */
    var chainepizzaItaliennes="";
    /* De meme mais les pizzas italiennes */

    desPizzas.forEach(unePizza =>  
        /* je vais parcourir desPizzas (une liste (Array) de pizza) et pour chaque pizza presente dans liste je vais effectuer les instructions a l'interieur des accolades.  
        unePizza fait réference a la pizza que je suis entrain de parcourir*/  
        /*desPizzas = Conteneur      unePizza = Contenu */
    {
    
        if(unePizza.nomGenre=="Nos classics")
        /* je regarde si le nom du genre de la pizza que je suis entrain de parcourir est egale à "Nos classics" */
        {
            chainePizzaClassics+="<div  class='unePizza' id='pizza"+unePizza.idPizza+"' ><span class='pizzaTitre'>"+unePizza.nomPizza+"</span><a href='./pizza.html?pizza="+unePizza.idPizza+"'><img class='imgPizza' src='./img/pizzaDefaut.jpg' alt='Une image de pizza'></a> </div>";
            /* Je concatene la chaine avec du code HTML et les infos de la pizza Nom ... */
        } /* Fin du Si */
        
    }); /* Fin de la boucle */

    divpizzasClassics.innerHTML=chainePizzaClassics;
    /* la div va recevoir le code HTML de la chaine */
} /* Fin de la procedure */

document.onload=affichePizzas(lesPizzas); 
/* Ici je crée un évenement ou déclencheur de ma procedure
Quand le document HTML est chargé alors j'utilise la procedure affichePizza qui reçoit en argument la variable lesPizzas ( voir data.sql ligne 227) */

